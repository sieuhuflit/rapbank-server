/* eslint-disable global-require,import/no-dynamic-require,no-bitwise */
const express = require('express');
const mongoose = require('mongoose');

const app = express();
const http = require('http');
const fs = require('fs');
const bodyParser = require('body-parser');
const path = require('path');
const config = require('config');

const server = http.createServer(app);

mongoose.Promise = global.Promise;
global.appRoot = path.resolve(__dirname);

mongoose.connect(config.get('DB_STRING'), { useMongoClient: true }, (err) => {
  if (err) {
    console.log(err);
  } else {
    console.log('Connected to the database: ', config.get('DB_STRING'));// eslint-disable-line
  }
});

const shopmodelsPath = `${__dirname}/app/models/`;
fs.readdirSync(shopmodelsPath).forEach((file) => {
  if (~file.indexOf('.js')) {// eslint-disable-line
    require(`${shopmodelsPath}/${file}`);// eslint-disable-line
  }
});

app.use(bodyParser.urlencoded({
  extended: true,
}));
app.use(bodyParser.json());
app.use('/api/', require('./app/routes'));

server.listen(config.get('API.PORT'), (err) => {
  if (err) {
    console.log(err);
  } else {
    console.log(`listening on port ${config.get('API.PORT')}`);
  }
});
