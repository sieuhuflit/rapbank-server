const express = require('express');

const router = express.Router();
const bankRoute = require('./components/bank');
const userRoute = require('./components/user');
const wantLoanRoute = require('./components/want-loan');
const giveLoanRoute = require('./components/give-loan');
const promotionRoute = require('./components/promotion');
const juristRoute = require('./components/jurist');

router.use('/bank', bankRoute);
router.use('/user', userRoute);
router.use('/want-loan', wantLoanRoute);
router.use('/give-loan', giveLoanRoute);
router.use('/promotion', promotionRoute);
router.use('/jurist', juristRoute);

module.exports = router;
