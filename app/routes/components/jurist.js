/*
Routing for Bank
*/
const express = require('express');

const router = express.Router();
const juristControllers = require('../../controllers/jurist');

router.post('/list', juristControllers.listJurist);

module.exports = router;
