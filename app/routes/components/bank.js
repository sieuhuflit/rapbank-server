/*
Routing for Bank
*/
const express = require('express');

const router = express.Router();
const bankControllers = require('../../controllers/bank');

router.post('/create', bankControllers.create);
router.post('/list', bankControllers.listBank);
router.get('/list-loan-type', bankControllers.listLoanType);
router.post('/find-loan', bankControllers.findLoan);
router.get('/list-saving-type', bankControllers.listSavingType);
router.post('/find-saving', bankControllers.findSaving);
module.exports = router;
