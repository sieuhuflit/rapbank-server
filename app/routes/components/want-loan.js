/*
Routing for Bank
*/
const express = require('express');

const router = express.Router();
const wantLoanControllers = require('../../controllers/wantLoan');

router.post('/create', wantLoanControllers.create);
router.post('/list', wantLoanControllers.listWantLoan);

module.exports = router;
