/*
Routing for Bank
*/
const express = require('express');

const router = express.Router();
const giveLoanControllers = require('../../controllers/giveLoan');

router.post('/create', giveLoanControllers.create);
router.post('/list', giveLoanControllers.listGiveLoan);

module.exports = router;
