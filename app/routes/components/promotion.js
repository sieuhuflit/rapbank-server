/*
Routing for Bank
*/
const express = require('express');

const router = express.Router();
const promotionControllers = require('../../controllers/promotion');

router.post('/list', promotionControllers.listPromotion);

module.exports = router;
