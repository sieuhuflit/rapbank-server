const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const BankSchema = new Schema({
  name: {
    type: String,
    default: '',
  },
  address: {
    type: String,
    default: '',
  },
  loan_percent: {
    type: Number,
    default: 0,
  },
  rating_star: {
    type: Number,
    default: 0,
  },
  created_at: {
    type: Number,
    default: Date.now(),
  },
});

module.exports = mongoose.model('Bank', BankSchema);
