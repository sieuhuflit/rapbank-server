const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const PromotionSchema = new Schema({
  bank_id: {
    type: Schema.Types.ObjectId,
    ref: 'Bank',
    required: true,
  },
  title: {
    type: String,
    default: '',
  },
  content: {
    type: String,
    default: '',
  },
  type: {
    type: String,
    default: '',
  },
  expired_date: {
    type: Number,
    default: Date.now(),
  },
  created_at: {
    type: Number,
    default: Date.now(),
  },
});

module.exports = mongoose.model('Promotion', PromotionSchema, 'promotions');
