const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const GiveLoanSchema = new Schema({
  user_id: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  amount: {
    type: Number,
    default: '',
  },
  month: {
    type: Number,
    default: 0,
  },
  expected_interest_rate: {
    type: Number,
    default: 0,
  },
  created_at: {
    type: Number,
    default: Date.now(),
  },
});

module.exports = mongoose.model('GiveLoan', GiveLoanSchema, 'give_loans');
