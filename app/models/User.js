const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const UserSchema = new Schema({
  username: {
    type: String,
    default: '',
  },
  password: {
    type: String,
    default: '',
  },
  address: {
    type: String,
    default: '',
  },
  email: {
    type: String,
    default: '',
  },
  image_url: {
    type: String,
    default: '',
  },
  connected_users: {
    type: Array,
    default: [],
  },
});

module.exports = mongoose.model('User', UserSchema, 'users');
