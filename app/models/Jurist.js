const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const JuristSchema = new Schema({
  fullname: {
    type: String,
    default: '',
  },
  city: {
    type: String,
    default: '',
  },
  rate: {
    type: Number,
    default: 0,
  },
  rep_time: {
    type: String,
    default: 0,
  },
  major: {
    type: String,
    default: '',
  },
  image_url: {
    type: String,
    default: 0,
  },
});

module.exports = mongoose.model('Jurist', JuristSchema, 'jurists');
