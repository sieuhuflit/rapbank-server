const mongoose = require('mongoose');
const Joi = require('joi');

const User = mongoose.model('User');
const code = require('../utils/code');
const message = require('../utils/message');
const ResponseHelper = require('../utils/ResponseHelper');

exports.profile = (req, res) => {
  const schema = Joi.object().keys({
    username: Joi.string().required(),
  });
  Joi.validate(req.body, schema, { allowUnknown: true }, (errValidate) => {
    if (errValidate) {
      return res.jsonp(ResponseHelper.responseError(
        code.MISSING_PARAMETER,
        message.MESSAGE_MISSING_PARAMETER,
      ));
    }
    return User.findOne({ username: req.body.username })
      .exec((errFindUser, foundUser) => {
        if (errFindUser) {
          return res.json(ResponseHelper.responseError(
            code.INTERNAL_SERVER_ERROR,
            message.MESSAGE_INTERNAL_SERVER_ERROR,
          ));
        }
        return res.jsonp(ResponseHelper.responseSuccess(code.SUCCESS, foundUser));
      });
  });
};

exports.login = (req, res) => {
  const schema = Joi.object().keys({
    username: Joi.string().required(),
    password: Joi.number().required(),
  });
  Joi.validate(req.body, schema, { allowUnknown: true }, (errValidate) => {
    if (errValidate) {
      return res.jsonp(ResponseHelper.responseError(
        code.MISSING_PARAMETER,
        message.MESSAGE_MISSING_PARAMETER,
      ));
    }
    return (
      User.findOne({ username: req.body.username })
        .exec((errFindUser, foundUser) => {
          if (errFindUser) {
            return res.json(ResponseHelper.responseError(
              code.INTERNAL_SERVER_ERROR,
              message.MESSAGE_INTERNAL_SERVER_ERROR,
            ));
          }
          if (foundUser) {
            if (foundUser.password === req.body.password) {
              return res.jsonp(ResponseHelper.responseSuccess(code.SUCCESS, foundUser));
            }
            return res.jsonp(ResponseHelper.responseError(
              code.WRONG_PASSWORD,
              message.MESSAGE_WRONG_PASSWORD,
            ));
          }
          return res.jsonp(ResponseHelper.responseError(
            code.USER_NOT_EXIST,
            message.MESSAGE_USER_NOT_EXIST,
          ));
        })
    );
  });
};
