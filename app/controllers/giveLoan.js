const mongoose = require('mongoose');
const Joi = require('joi');

const User = mongoose.model('User');
const GiveLoan = mongoose.model('GiveLoan');
const code = require('../utils/code');
const message = require('../utils/message');
const ResponseHelper = require('../utils/ResponseHelper');

exports.create = (req, res) => {
  const schema = Joi.object().keys({
    username: Joi.string().required(),
    amount: Joi.number().required(),
    month: Joi.number().required(),
    expected_interest_rate: Joi.number().required(),
  });
  Joi.validate(req.body, schema, { allowUnknown: true }, (errValidate) => {
    if (errValidate) {
      return res.jsonp(ResponseHelper.responseError(
        code.MISSING_PARAMETER,
        message.MESSAGE_MISSING_PARAMETER,
      ));
    }
    return User.findOne({ username: req.body.username })
      .exec((errFindUser, foundUser) => {
        const user = foundUser;
        const condition = {};
        condition.user_id = user.id;
        condition.amount = req.body.amount;
        condition.month = req.body.month;
        condition.created_at = Date.now();
        condition.expected_interest_rate = req.body.expected_interest_rate;
        return GiveLoan.create(condition, (errCreate, createdGiveLoan) => {
          res.jsonp(ResponseHelper.responseSuccess(code.SUCCESS, createdGiveLoan));
        });
      });
  });
};

exports.listGiveLoan = (req, res) => {
  const schema = Joi.object().keys({
    page: Joi.number().required(),
  });
  Joi.validate(req.body, schema, { allowUnknown: true }, (errValidate) => {
    if (errValidate) {
      return res.jsonp(ResponseHelper.responseError(
        code.MISSING_PARAMETER,
        message.MESSAGE_MISSING_PARAMETER,
      ));
    }
    const pageNumber = parseInt(req.body.page, 10);
    const skip = (pageNumber - 1) * 5;
    return GiveLoan.count()
      .exec((errCount, totalCount) => {
        if (errCount) {
          return res.jsonp(ResponseHelper.responseError(
            code.INTERNAL_SERVER_ERROR,
            message.MESSAGE_INTERNAL_SERVER_ERROR,
          ));
        }
        const totalPages = Math.ceil(totalCount / 5);
        return GiveLoan.find()
          .populate('user_id')
          .limit(5)
          .skip(skip)
          .sort('-created_at')
          .exec((errorFind, listBank) => {
            if (errorFind) {
              return res.jsonp(ResponseHelper.responseError(
                code.INTERNAL_SERVER_ERROR,
                message.MESSAGE_INTERNAL_SERVER_ERROR,
              ));
            }
            return res.jsonp({
              code: code.SUCCESS,
              data: listBank,
              page: pageNumber,
              totalCount,
              totalPages,
            });
          });
      });
  });
};
