const mongoose = require('mongoose');
const Joi = require('joi');

const Jurist = mongoose.model('Jurist');
const code = require('../utils/code');
const message = require('../utils/message');
const ResponseHelper = require('../utils/ResponseHelper');


function shuffle(array) {
  let currentIndex = array.length;
  let temporaryValue = array.length;
  let randomIndex = array.length;

  // While there remain elements to shuffle...
  while (currentIndex !== 0) {
    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

exports.listJurist = (req, res) => {
  const schema = Joi.object().keys({
    page: Joi.number().required(),
  });
  Joi.validate(req.body, schema, {allowUnknown: true}, (errValidate) => {
    if (errValidate) {
      return res.jsonp(ResponseHelper.responseError(
        code.MISSING_PARAMETER,
        message.MESSAGE_MISSING_PARAMETER,
      ));
    }
    const pageNumber = parseInt(req.body.page, 10);
    const skip = (pageNumber - 1) * 5;
    return Jurist.count()
      .exec((errCount, totalCount) => {
        if (errCount) {
          return res.jsonp(ResponseHelper.responseError(
            code.INTERNAL_SERVER_ERROR,
            message.MESSAGE_INTERNAL_SERVER_ERROR,
          ));
        }
        const totalPages = Math.ceil(totalCount / 5);
        return Jurist.find()
          .limit(5)
          .skip(skip)
          .exec((errorFind, listJurist) => {
            if (errorFind) {
              return res.jsonp(ResponseHelper.responseError(
                code.INTERNAL_SERVER_ERROR,
                message.MESSAGE_INTERNAL_SERVER_ERROR,
              ));
            }
            return res.jsonp({
              code: code.SUCCESS,
              data: shuffle(listJurist),
              page: pageNumber,
              totalCount,
              totalPages,
            });
          });
      });
  });
};
