const mongoose = require('mongoose');
const Joi = require('joi');

const Bank = mongoose.model('Bank');
const code = require('../utils/code');
const message = require('../utils/message');
const ResponseHelper = require('../utils/ResponseHelper');

function shuffle(array) {
  let currentIndex = array.length;
  let temporaryValue = array.length;
  let randomIndex = array.length;

  // While there remain elements to shuffle...
  while (currentIndex !== 0) {
    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}


exports.findSaving = (req, res) => {
  const schema = Joi.object().keys({
    amount: Joi.number().required(),
    month: Joi.number().required(),
  });
  Joi.validate(req.body, schema, { allowUnknown: true }, (errValidate) => {
    if (errValidate) {
      return res.jsonp(ResponseHelper.responseError(
        code.MISSING_PARAMETER,
        message.MESSAGE_MISSING_PARAMETER,
      ));
    }
    return Bank.find()
      .exec((errFindBank, foundBank) => {
        if (errFindBank) {
          return res.jsonp(ResponseHelper.responseError(
            code.INTERNAL_SERVER_ERROR,
            message.MESSAGE_INTERNAL_SERVER_ERROR,
          ));
        }
        return res.jsonp(ResponseHelper.responseSuccess(code.SUCCESS, shuffle(foundBank)));
      });
  });
};
exports.findLoan = (req, res) => {
  const schema = Joi.object().keys({
    amount: Joi.number().required(),
    term_amount: Joi.number().required(),
    expected_interest_rate: Joi.number().required(),
  });
  Joi.validate(req.body, schema, { allowUnknown: true }, (errValidate) => {
    if (errValidate) {
      return res.jsonp(ResponseHelper.responseError(
        code.MISSING_PARAMETER,
        message.MESSAGE_MISSING_PARAMETER,
      ));
    }
    return Bank.find()
      .exec((errFindBank, foundBank) => {
        if (errFindBank) {
          return res.jsonp(ResponseHelper.responseError(
            code.INTERNAL_SERVER_ERROR,
            message.MESSAGE_INTERNAL_SERVER_ERROR,
          ));
        }
        return res.jsonp(ResponseHelper.responseSuccess(code.SUCCESS, shuffle(foundBank)));
      });
  });
};

exports.create = (req, res) => {
  const schema = Joi.object().keys({
    name: Joi.string().required(),
    address: Joi.string().required(),
    loan_percent: Joi.number().required(),
  });
  Joi.validate(req.body, schema, { allowUnknown: true }, (errValidate) => {
    if (errValidate) {
      return res.jsonp(ResponseHelper.responseError(
        code.MISSING_PARAMETER,
        message.MESSAGE_MISSING_PARAMETER,
      ));
    }
    const condition = {};
    Bank.create(condition)
      .exec((errCreate, createdBank) =>
        res.jsonp(ResponseHelper.responseSuccess(code.SUCCESS, createdBank)));
  });
};

exports.listBank = (req, res) => {
  const schema = Joi.object().keys({
    page: Joi.number().required(),
  });
  Joi.validate(req.body, schema, { allowUnknown: true }, (errValidate) => {
    if (errValidate) {
      return res.jsonp(ResponseHelper.responseError(
        code.MISSING_PARAMETER,
        message.MESSAGE_MISSING_PARAMETER,
      ));
    }
    const pageNumber = parseInt(req.body.page, 10);
    const skip = (pageNumber - 1) * 5;
    return Bank.count()
      .exec((errCount, totalCount) => {
        if (errCount) {
          return res.jsonp(ResponseHelper.responseError(
            code.INTERNAL_SERVER_ERROR,
            message.MESSAGE_INTERNAL_SERVER_ERROR,
          ));
        }
        const totalPages = Math.ceil(totalCount / 5);
        return Bank.find()
          .limit(5)
          .skip(skip)
          .exec((errorFind, listBank) => {
            if (errorFind) {
              return res.jsonp(ResponseHelper.responseError(
                code.INTERNAL_SERVER_ERROR,
                message.MESSAGE_INTERNAL_SERVER_ERROR,
              ));
            }
            return res.jsonp({
              code: code.SUCCESS,
              data: listBank,
              page: pageNumber,
              totalCount,
              totalPages,
            });
          });
      });
  });
};

exports.listLoanType = (req, res) => {
  const arrServices = [
    {
      name: 'Vay tiêu dùng',
      description: 'Bạn đang có mong muốn trang trải việc tiêu dùng?',
      image_url: 'http://192.168.0.103/rapbank/loan_1.png',
    },
    {
      name: 'Vay mua nhà',
      description: 'Hệ thông cho vay mua nhà từ ngân hàng',
      image_url: 'http://192.168.0.103/rapbank/loan_2.png',
    },
    {
      name: 'Vay mua xe',
      description: 'Bạn đang có mong muốn mua xe?',
      image_url: 'http://192.168.0.103/rapbank/loan_3.png',
    },
    {
      name: 'Vay du học',
      description: 'Bạn có con em mong muốn đi du học',
      image_url: 'http://192.168.0.103/rapbank/loan_4.png',
    },
  ];
  return res.json(ResponseHelper.responseSuccess(
    code.SUCCESS,
    arrServices,
  ));
};

exports.listSavingType = (req, res) => {
  const arrServices = [
    {
      name: 'Gửi tiết kiệm ngày',
      description: 'Gửi tiết kiệm theo mỗi ngày',
      image_url: 'http://192.168.0.103/rapbank/saving_1.png',
    },
    {
      name: 'Gửi tiết kiệm tháng',
      description: 'Gửi tiết kiệm theo tháng',
      image_url: 'http://192.168.0.103/rapbank/saving_2.png',
    },
    {
      name: 'Gửi tiết kiệm quý',
      description: 'Gửi tiết kiệm theo từng quý',
      image_url: 'http://192.168.0.103/rapbank/saving_3.png',
    },
    {
      name: 'Gửi tiết kiệm năm',
      description: 'Gửi tiết kiệm theo năm',
      image_url: 'http://192.168.0.103/rapbank/saving_4.png',
    },
  ];
  return res.json(ResponseHelper.responseSuccess(
    code.SUCCESS,
    arrServices,
  ));
};
