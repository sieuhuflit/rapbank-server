const mongoose = require('mongoose');
const Joi = require('joi');

const Promotion = mongoose.model('Promotion');
const code = require('../utils/code');
const message = require('../utils/message');
const ResponseHelper = require('../utils/ResponseHelper');

exports.listPromotion = (req, res) => {
  const schema = Joi.object().keys({
    page: Joi.number().required(),
  });
  Joi.validate(req.body, schema, { allowUnknown: true }, (errValidate) => {
    if (errValidate) {
      return res.jsonp(ResponseHelper.responseError(
        code.MISSING_PARAMETER,
        message.MESSAGE_MISSING_PARAMETER,
      ));
    }
    const pageNumber = parseInt(req.body.page, 10);
    const skip = (pageNumber - 1) * 5;
    return Promotion.count()
      .exec((errCount, totalCount) => {
        if (errCount) {
          return res.jsonp(ResponseHelper.responseError(
            code.INTERNAL_SERVER_ERROR,
            message.MESSAGE_INTERNAL_SERVER_ERROR,
          ));
        }
        const totalPages = Math.ceil(totalCount / 5);
        return Promotion.find()
          .populate('bank_id')
          .limit(5)
          .skip(skip)
          .exec((errorFind, listBank) => {
            if (errorFind) {
              return res.jsonp(ResponseHelper.responseError(
                code.INTERNAL_SERVER_ERROR,
                message.MESSAGE_INTERNAL_SERVER_ERROR,
              ));
            }
            return res.jsonp({
              code: code.SUCCESS,
              data: listBank,
              page: pageNumber,
              totalCount,
              totalPages,
            });
          });
      });
  });
};
