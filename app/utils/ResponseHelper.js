function responseError(code, message) {
  return {
    code,
    message,
  };
}

function responseSuccess(code, data) {
  return {
    code,
    data,
  };
}

module.exports.responseSuccess = responseSuccess;
module.exports.responseError = responseError;
