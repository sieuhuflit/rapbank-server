module.exports = {
  MESSAGE_INTERNAL_SERVER_ERROR: 'Internal Server Error',
  MESSAGE_MISSING_PARAMETER: 'Missing parameter',
  MESSAGE_WRONG_PASSWORD: 'Wrong password',
  MESSAGE_USER_NOT_EXIST: 'User not exist',
};
