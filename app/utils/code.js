module.exports = {
  SUCCESS: 0,
  INTERNAL_SERVER_ERROR: 500,
  MISSING_PARAMETER: 1,
  WRONG_PASSWORD: 2,
  USER_NOT_EXIST: 3,
};
