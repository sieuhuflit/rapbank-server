module.exports = {
  name: 'rapbank-server-sit',
  script: './index.js',
  watch: true,
  instance_var: 'INSTANCE_ID',
  env_sit: {
    NODE_ENV: 'sit',
  },
};
